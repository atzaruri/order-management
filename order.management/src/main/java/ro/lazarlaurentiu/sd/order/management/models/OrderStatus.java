package ro.lazarlaurentiu.sd.order.management.models;

public enum OrderStatus {

	OPEN("Open"), CLOSED("Closed");

	private String orderStatus;

	private OrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderStatus() {
		return this.orderStatus;
	}

}

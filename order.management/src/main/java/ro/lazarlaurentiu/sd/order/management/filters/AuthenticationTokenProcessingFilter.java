package ro.lazarlaurentiu.sd.order.management.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import ro.lazarlaurentiu.sd.order.management.models.User;
import ro.lazarlaurentiu.sd.order.management.services.UserService;
import ro.lazarlaurentiu.sd.order.management.utils.TokenUtils;

@Component
public class AuthenticationTokenProcessingFilter extends GenericFilterBean {

	@Autowired
	private UserService userService;

	@Autowired
	private AuthenticationManager authenticationManager;

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		/* Remove authentication information from session */
		SecurityContextHolder.getContext().setAuthentication(null);

		if (request instanceof HttpServletRequest) {
			String token = ((HttpServletRequest) request).getHeader("Authorization-Token");
			if (token == null) ((HttpServletRequest) request).getHeader("authorization-token");
			
			if (token != null && TokenUtils.validateToken(userService, token)) {
				User user = TokenUtils.getUserFromToken(token);
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						user.getUsername(), user.getPassword());
				authentication
						.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));
				// set the authentication into the SecurityContext
				SecurityContextHolder.getContext()
						.setAuthentication(authenticationManager.authenticate(authentication));

			}
		}

		chain.doFilter(request, response);
	}

}

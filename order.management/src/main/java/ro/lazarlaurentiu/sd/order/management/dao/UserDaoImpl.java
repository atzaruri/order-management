package ro.lazarlaurentiu.sd.order.management.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ro.lazarlaurentiu.sd.order.management.models.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	private static final String USERNAME = "username";

	private static final Log LOGGER = LogFactory.getLog(UserDaoImpl.class);

	public User findByUserId(int userId) {
		return getByKey(userId);
	}

	public User findByUsername(String username) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("username", username));
		return (User) criteria.uniqueResult();
	}

	public int createUser(User user) {
		try {
			this.persist(user);
		} catch (Exception e) {
			LOGGER.error("Exception in createUser().", e);

			return -1;
		}

		return user.getUserId();
	}

	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		Criteria criteria = createEntityCriteria();
		criteria.addOrder(Order.asc(USERNAME));
		return criteria.list();
	}

	public boolean updateUser(User user) {
		try {
			this.update(user);
		} catch (Exception e) {
			LOGGER.error("Exception in updateUser().", e);

			return false;
		}

		return true;
	}

	public boolean deleteUser(User user) {
		try {
			this.delete(user);
		} catch (Exception e) {
			LOGGER.error("Exception in deleteUser().", e);

			return false;
		}

		return true;
	}

}

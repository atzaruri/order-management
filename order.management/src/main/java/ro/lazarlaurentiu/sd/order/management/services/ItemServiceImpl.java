package ro.lazarlaurentiu.sd.order.management.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.lazarlaurentiu.sd.order.management.dao.ItemDao;
import ro.lazarlaurentiu.sd.order.management.models.Category;
import ro.lazarlaurentiu.sd.order.management.models.Item;

@Service("itemService")
@Transactional
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemDao itemDao;

	public Item getItemById(int itemId) {
		return itemDao.getItemById(itemId);
	}

	public List<Item> getAllItems() {
		return itemDao.getAllItems();
	}

	public List<Item> getItemsForCategory(Category category) {
		return itemDao.getItemsForCategory(category);
	}

	public int createItem(Item item) {
		return itemDao.createItem(item);
	}

	public boolean updateItem(Item item) {
		return itemDao.updateItem(item);
	}

	public boolean deleteItem(Item item) {
		return itemDao.deleteItem(item);
	}

}

package ro.lazarlaurentiu.sd.order.management.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ro.lazarlaurentiu.sd.order.management.models.Order;

@Repository("orderDao")
public class OrderDaoImpl extends AbstractDao<Integer, Order> implements OrderDao {

	private static final String TABLE_NUMBER = "tableNumber";

	private static final String ORDER_STATUS = "orderStatus";

	private static final Log LOGGER = LogFactory.getLog(OrderDaoImpl.class);

	public Order getOrderByNumber(int orderNumber) {
		return this.getByKey(orderNumber);
	}

	public Order getOpenOrderForTable(int tableNumber) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq(TABLE_NUMBER, tableNumber));
		criteria.add(Restrictions.eq(ORDER_STATUS, "Open"));
		return (Order) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Order> getAllOpenOrders() {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq(ORDER_STATUS, "Open"));
		return criteria.list();
	}

	public int createOrder(Order order) {
		try {
			this.persist(order);
		} catch (Exception e) {
			LOGGER.error("Exception in createOrder().", e);

			return -1;
		}

		return order.getOrderNumber();
	}

	public boolean updateOrder(Order order) {
		try {
			this.update(order);
		} catch (Exception e) {
			LOGGER.error("Exception in updateOrder().", e);

			return false;
		}

		return true;
	}

	public boolean deleteOrder(Order order) {
		try {
			this.delete(order);
		} catch (Exception e) {
			LOGGER.error("Exception in deleteOrder().", e);

			return false;
		}

		return true;
	}

}

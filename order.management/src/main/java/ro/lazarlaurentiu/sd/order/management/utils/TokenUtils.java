package ro.lazarlaurentiu.sd.order.management.utils;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

import org.apache.commons.codec.digest.DigestUtils;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import ro.lazarlaurentiu.sd.order.management.models.User;
import ro.lazarlaurentiu.sd.order.management.services.UserService;

public class TokenUtils {

	private static SecureRandom secureRandom = new SecureRandom();

	public static String getToken(User user) {
		if (user != null) {
			String tokenKey = Long.toString(secureRandom.nextLong());

			String decodedToken = user.getUsername() + ":" + tokenKey + ":" + user.getPassword() + ":"
					+ DigestUtils.md5Hex(tokenKey);

			return Base64.encode(decodedToken.getBytes());
		}

		return null;
	}

	public static boolean validateToken(UserService userService, String token) {
		byte[] tokenBytes = Base64.decode(token);

		if (tokenBytes != null) {
			String actualToken = new String(tokenBytes, StandardCharsets.UTF_8);
			String[] tokenParts = actualToken.split(":");

			if (tokenParts.length == 4 && DigestUtils.md5Hex(tokenParts[1]).equals(tokenParts[3])) {
				User user = userService.findByUsername(tokenParts[0]);

				if (user != null && tokenParts[2].equals(user.getPassword())) {
					return true;
				}
			}
		}

		return false;
	}

	public static User getUserFromToken(String token) {
		byte[] tokenBytes = Base64.decode(token);

		if (tokenBytes != null) {
			String actualToken = new String(tokenBytes);
			String[] tokenParts = actualToken.split(":");

			if (tokenParts.length == 4) {
				User user = new User();
				user.setUsername(tokenParts[0]);
				user.setPassword(tokenParts[2]);

				return user;
			}
		}

		return null;
	}

}

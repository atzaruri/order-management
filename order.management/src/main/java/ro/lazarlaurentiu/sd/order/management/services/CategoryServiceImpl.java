package ro.lazarlaurentiu.sd.order.management.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.lazarlaurentiu.sd.order.management.dao.CategoryDao;
import ro.lazarlaurentiu.sd.order.management.models.Category;

@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDao categoryDao;

	public Category getCateoryById(int categoryId) {
		return categoryDao.getCateoryById(categoryId);
	}

	public List<Category> getAllCategories() {
		return categoryDao.getAllCategories();
	}

	public int createCategory(Category category) {
		return categoryDao.createCategory(category);
	}

	public boolean updateCategory(Category category) {
		return categoryDao.updateCategory(category);
	}

	public boolean deleteCategory(Category category) {
		return categoryDao.deleteCategory(category);
	}

}

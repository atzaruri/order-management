package ro.lazarlaurentiu.sd.order.management.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ro.lazarlaurentiu.sd.order.management.models.Category;
import ro.lazarlaurentiu.sd.order.management.models.Item;

@Repository("itemDao")
public class ItemDaoImpl extends AbstractDao<Integer, Item> implements ItemDao {

	private static final String ITEM_NAME = "itemName";

	private static final String ITEM_CATEGORY = "itemCategory";

	private static final Log LOGGER = LogFactory.getLog(ItemDaoImpl.class);
	
	public Item getItemById(int itemId) {
		return this.getByKey(itemId);
	}
	
	@SuppressWarnings("unchecked")
	public List<Item> getAllItems() {
		Criteria criteria = createEntityCriteria();
		criteria.addOrder(Order.asc(ITEM_NAME));
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Item> getItemsForCategory(Category category) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq(ITEM_CATEGORY, category));
		return criteria.list();
	}

	public int createItem(Item item) {
		try {
			this.persist(item);
		} catch (Exception e) {
			LOGGER.error("Exception in createItem().", e);

			return -1;
		}

		return item.getItemId();
	}

	public boolean updateItem(Item item) {
		try {
			this.update(item);
		} catch (Exception e) {
			LOGGER.error("Exception in updateItem().", e);

			return false;
		}

		return true;
	}

	public boolean deleteItem(Item item) {
		try {
			this.delete(item);
		} catch (Exception e) {
			LOGGER.error("Exception in deleteItem().", e);

			return false;
		}

		return true;
	}

}

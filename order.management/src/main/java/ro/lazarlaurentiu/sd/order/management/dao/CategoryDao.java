package ro.lazarlaurentiu.sd.order.management.dao;

import java.util.List;

import ro.lazarlaurentiu.sd.order.management.models.Category;

public interface CategoryDao {

	public Category getCateoryById(int categoryId);

	public List<Category> getAllCategories();

	public int createCategory(Category category);

	public boolean updateCategory(Category category);

	public boolean deleteCategory(Category category);

}

package ro.lazarlaurentiu.sd.order.management.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ro.lazarlaurentiu.sd.order.management.models.User;
import ro.lazarlaurentiu.sd.order.management.services.UserService;

@RestController
public class UsersController {

	@Autowired
	private UserService userService;

	private static final String SUCCESS = "success";

	@RequestMapping(value = "/admin/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	@RequestMapping(value = "/admin/users/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public User getUserById(@PathVariable int userId) {
		return userService.findByUserId(userId);
	}

	@RequestMapping(value = "/admin/users/username={username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public User getUserByUsername(@PathVariable String username) {
		return userService.findByUsername(username);
	}

	@RequestMapping(value = "/admin/users/{userId}/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Boolean> deleteUser(@PathVariable int userId) {
		User user = userService.findByUserId(userId);
		boolean success = false;

		if (user != null) {
			success = userService.deleteUser(user);
		}

		return Collections.singletonMap(SUCCESS, success);
	}

}

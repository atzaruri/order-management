package ro.lazarlaurentiu.sd.order.management.services;

import java.util.List;

import ro.lazarlaurentiu.sd.order.management.models.Order;

public interface OrderService {

	public Order getOrderByNumber(int orderNumber);

	public Order getOpenOrderForTable(int tableNumber);
	
	public List<Order> getAllOpenOrders();
	
	public int createOrder(Order order);
	
	public boolean updateOrder(Order order);
	
	public boolean deleteOrder(Order order);

}

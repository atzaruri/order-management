package ro.lazarlaurentiu.sd.order.management.dao;

import java.util.List;

import ro.lazarlaurentiu.sd.order.management.models.User;

public interface UserDao {

	public User findByUserId(int userId);

	public User findByUsername(String username);
	
	public List<User> getAllUsers();
	
	public int createUser(User user);
	
	public boolean updateUser(User user);
	
	public boolean deleteUser(User user);

}

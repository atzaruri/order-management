package ro.lazarlaurentiu.sd.order.management.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ro.lazarlaurentiu.sd.order.management.models.Category;
import ro.lazarlaurentiu.sd.order.management.models.Item;
import ro.lazarlaurentiu.sd.order.management.services.CategoryService;
import ro.lazarlaurentiu.sd.order.management.services.ItemService;

@RestController
public class ItemsController {

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ItemService itemService;

	private static final String SUCCESS = "success";
	private static final String ITEM_ID = "itemId";

	@RequestMapping(value = "/items", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Item> getAllItems() {
		return itemService.getAllItems();
	}

	@RequestMapping(value = "/items/category/{categoryId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Item> getItemsForCategory(@PathVariable int categoryId) {
		Category category = categoryService.getCateoryById(categoryId);

		if (category != null) {
			return itemService.getItemsForCategory(category);
		}

		return new ArrayList<Item>();
	}

	@RequestMapping(value = "/admin/items", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> createItem(@RequestParam String itemName, @RequestParam int categoryId,
			@RequestParam double itemPrice) {
		Category category = new Category();
		category.setCategoryId(categoryId);

		Item item = new Item();
		item.setItemName(itemName);
		item.setItemCategory(category);
		item.setItemPrice(itemPrice);

		int itemId = itemService.createItem(item);

		HashMap<String, Object> resultsMap = new HashMap<String, Object>();
		resultsMap.put(SUCCESS, itemId > 0 ? true : false);
		resultsMap.put(ITEM_ID, itemId);

		return resultsMap;
	}

	@RequestMapping(value = "/admin/items/{itemId}/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Boolean> updateItem(@PathVariable int itemId, @RequestParam String itemName,
			@RequestParam int categoryId, @RequestParam double itemPrice) {
		Item item = itemService.getItemById(itemId);
		boolean success = false, mustUpdate = false;

		if (item != null) {
			if (!item.getItemName().equals(itemName)) {
				item.setItemName(itemName);
				mustUpdate = true;
			}
			if (item.getItemCategory().getCategoryId() != categoryId) {
				item.setItemCategory(categoryService.getCateoryById(categoryId));
				mustUpdate = true;
			}
			if (item.getItemPrice() != itemPrice) {
				item.setItemPrice(itemPrice);
				mustUpdate = true;
			}
			if (mustUpdate) {
				success = itemService.updateItem(item);
			} else {
				success = true;
			}
		}

		return Collections.singletonMap(SUCCESS, success);
	}

	@RequestMapping(value = "/admin/items/{itemId}/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Boolean> createItem(@PathVariable int itemId) {
		Item item = itemService.getItemById(itemId);
		boolean success = false;

		if (item != null) {
			success = itemService.deleteItem(item);
		}

		return Collections.singletonMap(SUCCESS, success);
	}

}

package ro.lazarlaurentiu.sd.order.management.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ro.lazarlaurentiu.sd.order.management.models.Item;
import ro.lazarlaurentiu.sd.order.management.models.Order;
import ro.lazarlaurentiu.sd.order.management.models.OrderItem;
import ro.lazarlaurentiu.sd.order.management.models.OrderItemPk;
import ro.lazarlaurentiu.sd.order.management.services.OrderItemService;

@RestController
public class OrderItemsController {

	@Autowired
	private OrderItemService orderItemService;

	private static final String SUCCESS = "success";

	@RequestMapping(value = "/orders/{orderNumber}/items", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<OrderItem> getItemsForOrder(@PathVariable int orderNumber) {
		Order order = new Order();
		order.setOrderNumber(orderNumber);

		return orderItemService.getItemsForOrder(order);
	}

	@RequestMapping(value = "/order_items", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Boolean> createOrUpdateOrderItem(@RequestParam int orderNumber, @RequestParam int itemId,
			@RequestParam int quantity) {
		Order order = new Order();
		order.setOrderNumber(orderNumber);

		Item item = new Item();
		item.setItemId(itemId);

		OrderItemPk pk = new OrderItemPk(order, item);

		OrderItem orderItem = orderItemService.getOrderItemByKey(pk);
		if (orderItem == null) {
			orderItem = new OrderItem();
			orderItem.setPk(pk);
			orderItem.setQuantity(quantity);
			return Collections.singletonMap(SUCCESS, orderItemService.createOrderItem(orderItem));
		}

		orderItem.setQuantity(orderItem.getQuantity() + quantity);

		return Collections.singletonMap(SUCCESS, orderItemService.updateOrderItem(orderItem));
	}

	@RequestMapping(value = "/orders/{orderNumber}/items/{itemId}/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Boolean> createOrder(@PathVariable int orderNumber, @PathVariable int itemId) {
		Order order = new Order();
		order.setOrderNumber(orderNumber);

		Item item = new Item();
		item.setItemId(itemId);

		OrderItemPk pk = new OrderItemPk(order, item);

		OrderItem orderItem = new OrderItem();
		orderItem.setPk(pk);

		return Collections.singletonMap(SUCCESS, orderItemService.deleteOrderItem(orderItem));
	}

}

package ro.lazarlaurentiu.sd.order.management.models;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Embeddable
public class OrderItemPk implements Serializable {

	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "orderId")
	private Order order;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "itemId")
	private Item item;

	private static final long serialVersionUID = 1L;

	public OrderItemPk() {
	}

	public OrderItemPk(Order order, Item item) {
		this.order = order;
		this.item = item;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	@Override
	public boolean equals(Object object) {
		if (this == object)
			return true;
		if (object == null || getClass() != object.getClass())
			return false;

		OrderItemPk that = (OrderItemPk) object;

		if (order != null ? !order.equals(that.order) : that.order != null)
			return false;
		if (item != null ? !item.equals(that.item) : that.item != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int hashCode;

		hashCode = order != null ? order.hashCode() : 0;
		hashCode = 31 * hashCode() + (item != null ? item.hashCode() : 0);

		return hashCode;
	}

}

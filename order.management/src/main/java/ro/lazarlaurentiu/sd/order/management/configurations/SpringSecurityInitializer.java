package ro.lazarlaurentiu.sd.order.management.configurations;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Registers the Spring Security Filter.
 * 
 * @author Lazar Laurentiu
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}

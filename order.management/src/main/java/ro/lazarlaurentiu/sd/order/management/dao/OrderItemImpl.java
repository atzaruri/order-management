package ro.lazarlaurentiu.sd.order.management.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ro.lazarlaurentiu.sd.order.management.models.Order;
import ro.lazarlaurentiu.sd.order.management.models.OrderItem;
import ro.lazarlaurentiu.sd.order.management.models.OrderItemPk;

@Repository("orderItemDao")
public class OrderItemImpl extends AbstractDao<OrderItemPk, OrderItem> implements OrderItemDao {

	private static final String PK_ORDER = "pk.order";

	private static final Log LOGGER = LogFactory.getLog(OrderItemImpl.class);

	public OrderItem getOrderItemByKey(OrderItemPk pk) {
		return this.getByKey(pk);
	}

	@SuppressWarnings("unchecked")
	public List<OrderItem> getItemsForOrder(Order order) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq(PK_ORDER, order));
		return criteria.list();
	}

	public boolean createOrderItem(OrderItem orderItem) {
		try {
			this.persist(orderItem);
		} catch (Exception e) {
			LOGGER.error("Exception in createOrderItem()", e);

			return false;
		}

		return true;
	}

	public boolean updateOrderItem(OrderItem orderItem) {
		try {
			this.update(orderItem);
		} catch (Exception e) {
			LOGGER.error("Exception in updateOrderItem().", e);

			return false;
		}

		return true;
	}

	public boolean deleteOrderItem(OrderItem orderItem) {
		try {
			this.delete(orderItem);
		} catch (Exception e) {
			LOGGER.error("Exception in deleteOrderItem()", e);

			return false;
		}

		return true;
	}

}

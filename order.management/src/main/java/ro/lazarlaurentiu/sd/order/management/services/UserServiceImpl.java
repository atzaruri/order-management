package ro.lazarlaurentiu.sd.order.management.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.lazarlaurentiu.sd.order.management.dao.UserDao;
import ro.lazarlaurentiu.sd.order.management.models.User;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public User findByUserId(int userId) {
		return userDao.findByUserId(userId);
	}

	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}
	
	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}
	
	public int createUser(User user) {
		return userDao.createUser(user);
	}

	public boolean updateUser(User user) {
		return userDao.updateUser(user);
	}

	public boolean deleteUser(User user) {
		return userDao.deleteUser(user);
	}

}

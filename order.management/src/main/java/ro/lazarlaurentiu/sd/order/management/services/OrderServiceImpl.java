package ro.lazarlaurentiu.sd.order.management.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.lazarlaurentiu.sd.order.management.dao.OrderDao;
import ro.lazarlaurentiu.sd.order.management.models.Order;

@Service("orderService")
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDao orderDao;

	public Order getOrderByNumber(int orderNumber) {
		return orderDao.getOrderByNumber(orderNumber);
	}

	public List<Order> getAllOpenOrders() {
		return orderDao.getAllOpenOrders();
	}

	public Order getOpenOrderForTable(int tableNumber) {
		return orderDao.getOpenOrderForTable(tableNumber);
	}

	public int createOrder(Order order) {
		return orderDao.createOrder(order);
	}

	public boolean updateOrder(Order order) {
		return orderDao.updateOrder(order);
	}

	public boolean deleteOrder(Order order) {
		return orderDao.deleteOrder(order);
	}

}

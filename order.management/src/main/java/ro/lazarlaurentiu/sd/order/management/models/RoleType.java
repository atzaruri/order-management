package ro.lazarlaurentiu.sd.order.management.models;

public enum RoleType {

	ADMIN("Admin"), WAITER("Waiter");

	private String roleType;

	private RoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getRoleType() {
		return this.roleType;
	}

	public static RoleType getRole(String roleType) {
		for (RoleType role : RoleType.values()) {
			if (role.getRoleType().equals(roleType)) {
				return role;
			}
		}

		return null;
	}

}

package ro.lazarlaurentiu.sd.order.management.controllers;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ro.lazarlaurentiu.sd.order.management.models.Order;
import ro.lazarlaurentiu.sd.order.management.models.OrderItem;
import ro.lazarlaurentiu.sd.order.management.models.OrderStatus;
import ro.lazarlaurentiu.sd.order.management.models.User;
import ro.lazarlaurentiu.sd.order.management.services.OrderItemService;
import ro.lazarlaurentiu.sd.order.management.services.OrderService;
import ro.lazarlaurentiu.sd.order.management.services.UserService;

@RestController
public class OrdersControllers {

	@Autowired
	private UserService userService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrderItemService orderItemService;

	private static final String ORDER_NUMBER = "orderNumber";

	private static final String SUCCESS = "success";
	private static final String TOTAL = "total";

	@RequestMapping(value = "/orders", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> createOrder(@RequestParam int tableNumber,
			@RequestParam(required = false) String orderStatus) {
		User user = getCurrentUser();

		Order order = new Order();
		order.setUser(user);
		order.setTableNumber(tableNumber);
		if (orderStatus != null) {
			order.setOrderStatus(orderStatus);
		}

		orderService.createOrder(order);

		HashMap<String, Object> resultsMap = new HashMap<String, Object>();
		resultsMap.put(SUCCESS, order.getOrderNumber() > 0 ? true : false);
		resultsMap.put(ORDER_NUMBER, order.getOrderNumber());

		return resultsMap;
	}

	@RequestMapping(value = "/orders/table/{tableNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Order getOpenOrderForTable(@PathVariable int tableNumber) {
		Order order = orderService.getOpenOrderForTable(tableNumber);
		if (order != null) {
			order.setTotal(getOrderTotal(order));
		}

		return order;
	}

	@RequestMapping(value = "/orders/open", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Order> getOpenOrderForTable() {
		return orderService.getAllOpenOrders();
	}

	@RequestMapping(value = "/orders/{orderNumber}/total", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Double> getOrderTotal(@PathVariable int orderNumber) {
		Order order = new Order();
		order.setOrderNumber(orderNumber);

		return Collections.singletonMap(TOTAL, getOrderTotal(order));
	}

	@RequestMapping(value = "/orders/{orderNumber}/close", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> closeOrder(@PathVariable int orderNumber) {
		Order order = orderService.getOrderByNumber(orderNumber);
		order.setOrderStatus(OrderStatus.CLOSED.getOrderStatus());
		order.setOrderTimestamp(Calendar.getInstance().getTime());

		boolean success = orderService.updateOrder(order);

		Map<String, Object> result = new HashMap<String, Object>();
		result.put(SUCCESS, success);
		result.put(TOTAL, getOrderTotal(order));

		return result;
	}

	@RequestMapping(value = "/orders/{orderNumber}/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Boolean> deleteOrder(@PathVariable int orderNumber) {
		Order order = new Order();
		order.setOrderNumber(orderNumber);

		return Collections.singletonMap(SUCCESS, orderService.deleteOrder(order));
	}

	private double getOrderTotal(Order order) {
		List<OrderItem> orderItems = orderItemService.getItemsForOrder(order);

		double total = 0.0;

		if (orderItems != null) {
			for (OrderItem orderItem : orderItems) {
				total += orderItem.getPk().getItem().getItemPrice() * orderItem.getQuantity();
			}
		}

		return total;
	}

	private User getCurrentUser() {
		String username = ((org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();

		return userService.findByUsername(username);
	}

}

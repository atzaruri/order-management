package ro.lazarlaurentiu.sd.order.management.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.lazarlaurentiu.sd.order.management.dao.OrderItemDao;
import ro.lazarlaurentiu.sd.order.management.models.Order;
import ro.lazarlaurentiu.sd.order.management.models.OrderItem;
import ro.lazarlaurentiu.sd.order.management.models.OrderItemPk;

@Service("orderItemService")
@Transactional
public class OrderItemServiceImpl implements OrderItemService {

	@Autowired
	private OrderItemDao orderItemDao;

	public OrderItem getOrderItemByKey(OrderItemPk pk) {
		return orderItemDao.getOrderItemByKey(pk);
	}

	public List<OrderItem> getItemsForOrder(Order order) {
		return orderItemDao.getItemsForOrder(order);
	}

	public boolean createOrderItem(OrderItem orderItem) {
		return orderItemDao.createOrderItem(orderItem);
	}

	public boolean updateOrderItem(OrderItem orderItem) {
		return orderItemDao.updateOrderItem(orderItem);
	}

	public boolean deleteOrderItem(OrderItem orderItem) {
		return orderItemDao.deleteOrderItem(orderItem);
	}

}

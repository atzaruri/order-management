package ro.lazarlaurentiu.sd.order.management.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import ro.lazarlaurentiu.sd.order.management.models.Role;
import ro.lazarlaurentiu.sd.order.management.models.RoleType;
import ro.lazarlaurentiu.sd.order.management.models.User;
import ro.lazarlaurentiu.sd.order.management.services.UserService;
import ro.lazarlaurentiu.sd.order.management.utils.TokenUtils;

@RestController
public class AuthenticationController {

	@Autowired
	private UserService userService;

	private static final String SUCCESS = "success";
	private static final String NAME = "name";
	private static final String ROLE = "role";
	private static final String TOKEN = "token";

	@RequestMapping(value = "/authentication", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, String> login(@RequestParam String username, @RequestParam String password,
			HttpServletResponse response) throws JsonProcessingException {
		User user = userService.findByUsername(username);
		String token = null;

		if (user != null && user.getPassword().equals(DigestUtils.md5Hex(password))) {
			token = TokenUtils.getToken(user);
		}

		response.setStatus(HttpServletResponse.SC_OK);

		Map<String, String> responseData = new HashMap<String, String>();
		responseData.put(SUCCESS, "success");
		responseData.put(TOKEN, token);
		responseData.put(NAME, user.getName());
		responseData.put(ROLE, getUserRole(user));

		return responseData;
	}

	private String getUserRole(User user) {
		for (Role role : user.getUserRoles()) {
			if (role.getRoleType().equals(RoleType.ADMIN.getRoleType())) {
				return RoleType.ADMIN.getRoleType();
			}
		}

		return RoleType.WAITER.getRoleType();
	}

}

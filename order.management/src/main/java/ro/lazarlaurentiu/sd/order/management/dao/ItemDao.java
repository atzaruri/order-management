package ro.lazarlaurentiu.sd.order.management.dao;

import java.util.List;

import ro.lazarlaurentiu.sd.order.management.models.Category;
import ro.lazarlaurentiu.sd.order.management.models.Item;

public interface ItemDao {
	
	public Item getItemById(int itemId);

	public List<Item> getAllItems();

	public List<Item> getItemsForCategory(Category category);

	public int createItem(Item item);
	
	public boolean updateItem(Item item);
	
	public boolean deleteItem(Item item);
	
}

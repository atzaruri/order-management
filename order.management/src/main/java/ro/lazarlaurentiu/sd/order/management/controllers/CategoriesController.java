package ro.lazarlaurentiu.sd.order.management.controllers;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ro.lazarlaurentiu.sd.order.management.models.Category;
import ro.lazarlaurentiu.sd.order.management.services.CategoryService;

@RestController
public class CategoriesController {

	@Autowired
	private CategoryService categoryService;

	private static final String SUCCESS = "success";
	private static final String CATEGORY_ID = "categoryId";

	@RequestMapping(value = "/categories/{categoryId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Category getCategoryById(@PathVariable int categoryId) {
		return categoryService.getCateoryById(categoryId);
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Category> getAllCategories() {
		return categoryService.getAllCategories();
	}

	@RequestMapping(value = "/admin/categories", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> createCategory(@RequestParam String categoryType) {
		Category category = new Category();
		category.setCategoryType(categoryType);

		int categoryId = categoryService.createCategory(category);

		Map<String, Object> result = new HashMap<String, Object>();
		result.put(SUCCESS, categoryId > 0 ? true : false);
		result.put(CATEGORY_ID, categoryId);

		return result;
	}

	@RequestMapping(value = "/admin/categories/{categoryId}/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Boolean> updateCategory(@PathVariable int categoryId, @RequestParam String categoryType) {
		Category category = categoryService.getCateoryById(categoryId);
		boolean success = false;
		boolean mustUpdate = false;

		if (category != null) {
			if (!category.getCategoryType().equals(categoryType)) {
				category.setCategoryType(categoryType);
				mustUpdate = true;
			}
			if (mustUpdate) {
				success = categoryService.updateCategory(category);
			} else {
				success = true;
			}
		}

		return Collections.singletonMap(SUCCESS, success);
	}

	@RequestMapping(value = "/admin/categories/{categoryId}/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Boolean> deleteCategory(@PathVariable int categoryId) {
		Category category = categoryService.getCateoryById(categoryId);
		boolean success = false;

		if (category != null) {
			success = categoryService.deleteCategory(category);
		}

		return Collections.singletonMap(SUCCESS, success);
	}
}

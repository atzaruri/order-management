package ro.lazarlaurentiu.sd.order.management.dao;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import ro.lazarlaurentiu.sd.order.management.models.Category;

@Repository("categoryDao")
public class CategoryDaoImpl extends AbstractDao<Integer, Category> implements CategoryDao {

	private static final String CATEGORY_TYPE = "categoryType";

	private static final Log LOGGER = LogFactory.getLog(CategoryDaoImpl.class);

	public Category getCateoryById(int categoryId) {
		return this.getByKey(categoryId);
	}

	@SuppressWarnings("unchecked")
	public List<Category> getAllCategories() {
		Criteria criteria = createEntityCriteria();
		criteria.addOrder(Order.asc(CATEGORY_TYPE));
		return criteria.list();
	}

	public int createCategory(Category category) {
		try {
			this.persist(category);
		} catch (Exception e) {
			LOGGER.error("Exception in createCategory().", e);

			return -1;
		}

		return category.getCategoryId();
	}

	public boolean updateCategory(Category category) {
		try {
			this.update(category);
		} catch (Exception e) {
			LOGGER.error("Exception in updateCategory().", e);

			return false;
		}

		return true;
	}

	public boolean deleteCategory(Category category) {
		try {
			this.delete(category);
		} catch (Exception e) {
			LOGGER.error("Exception in deleteCategory().", e);

			return false;
		}

		return true;
	}

}

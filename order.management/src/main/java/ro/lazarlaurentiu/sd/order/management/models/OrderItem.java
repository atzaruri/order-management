package ro.lazarlaurentiu.sd.order.management.models;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "order_items")
@AssociationOverrides({
		@AssociationOverride(name = "pk.order", joinColumns = @JoinColumn(name = "orderNumber")),
		@AssociationOverride(name = "pk.item", joinColumns = @JoinColumn(name = "itemId"))
})
public class OrderItem {

	@EmbeddedId
	private OrderItemPk pk;

	@Column(name = "quantity")
	private int quantity;
	
	public OrderItemPk getPk() {
		return pk;
	}

	public void setPk(OrderItemPk pk) {
		this.pk = pk;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}

package ro.lazarlaurentiu.sd.order.management.services;

import java.util.List;

import ro.lazarlaurentiu.sd.order.management.models.Category;

public interface CategoryService {
	
	public Category getCateoryById(int categoryId);
	
	public List<Category> getAllCategories();
	
	public int createCategory(Category category);

	public boolean updateCategory(Category category);

	public boolean deleteCategory(Category category);

}

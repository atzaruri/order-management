package ro.lazarlaurentiu.sd.order.management.dao;

import java.util.List;

import ro.lazarlaurentiu.sd.order.management.models.Order;
import ro.lazarlaurentiu.sd.order.management.models.OrderItem;
import ro.lazarlaurentiu.sd.order.management.models.OrderItemPk;

public interface OrderItemDao {
	
	public OrderItem getOrderItemByKey(OrderItemPk pk);
	
	public List<OrderItem> getItemsForOrder(Order order);

	public boolean createOrderItem(OrderItem orderItem);

	public boolean updateOrderItem(OrderItem orderItem);

	public boolean deleteOrderItem(OrderItem orderItem);

}

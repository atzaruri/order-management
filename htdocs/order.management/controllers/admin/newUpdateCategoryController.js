app.controller('newUpdateCategoryController', function ($scope, $rootScope, $http, ngDialog) {
	$scope.token = JSON.parse(sessionStorage.token);
	
	$scope.category = {};

	var categoryIndex = $rootScope.categoryIndex;
	$rootScope.categoryIndex = null;
	if (categoryIndex != null) {
		$scope.category.categoryId   = $rootScope.categories[categoryIndex].categoryId;
		$scope.category.categoryType = $rootScope.categories[categoryIndex].categoryType;
	}

	$scope.submit = function() {
		if ($scope.formValid) {
			if (categoryIndex != null & categoryIndex > -1) {
				updateCategory ();
			} else {
				createCategory ();
			}
		}
	};

	function createCategory () {
		$http({
			method : 'POST',
			url : 'http://localhost:8080/order.management/admin/categories',
			data : $.param($scope.category),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded',
				'Accept' : 'application/json',
				'Authorization-Token' : $scope.token
			}
		}).success(function(data, status, headers, config) {
			if (data.success) {
				$scope.category.categoryId = data.categoryId
				$rootScope.categories.push($scope.category);
				ngDialog.close();
			} else {
				$scope.message = 'Category creation failed';
				$scope.showErrorMessage = true;
			}
		}).error(function(response, status, headers, config) {
			$scope.message = 'Category creation failed';
			$scope.showErrorMessage = true;
		});
	}

	function updateCategory () {
		$http({
			method : 'POST',
			url : 'http://localhost:8080/order.management/admin/categories/' + $scope.category.categoryId + '/update',
			data : $.param($scope.category),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded',
				'Accept' : 'application/json',
				'Authorization-Token' : $scope.token
			}
		}).success(function(data, status, headers, config) {
			if (data.success) {
				$rootScope.categories[categoryIndex].categoryType = $scope.category.categoryType;
				ngDialog.close();
			} else {
				$scope.message = 'Category update failed';
				$scope.showErrorMessage = true;
			}
		}).error(function(response, status, headers, config) {
			$scope.message = 'Category update failed';
			$scope.showErrorMessage = true;
		});
	}

	$scope.cancel = function () {
		ngDialog.close();
	};
});
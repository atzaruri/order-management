app.controller('categoriesController', function ($scope, $rootScope, $http, ngDialog) {
	$scope.token = JSON.parse(sessionStorage.token);
	$rootScope.categoryIndex = null;

	/* Get categories ------------------------------------------------------------------------------------------------------ */
	$http({
		method : 'GET',
		url : 'http://localhost:8080/order.management/categories',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded',
			'Accept' : 'application/json',
			'Authorization-Token' : $scope.token
		}
	}).success(function (data, status, headers, config) {
		$rootScope.categories = data;
	}).error(function (response, status, headers, config) {
		$rootScope.categories = [];
		$scope.message = 'An error occured while fetching all categories!';
		$scope.showErrorMessage = true;
	});

	$scope.openNewCateogry = function () {
        ngDialog.open({ template: '/order.management/admin/newUpdateCategory.html', controller: 'newUpdateCategoryController' });
    };

    $scope.openUpdateCategory = function (categoryId) {
    	$rootScope.categoryIndex = getCategoryIndex (categoryId);
        ngDialog.open({ template: '/order.management/admin/newUpdateCategory.html', controller: 'newUpdateCategoryController' });
    };


	$scope.delete = function (categoryId) {
		$scope.showErrorMessage = false;
		var categoryIndex = getCategoryIndex(categoryId);

		if (categoryIndex >= 0) {
			$http({
				method : 'POST',
				url : 'http://localhost:8080/order.management/admin/categories/' + categoryId + "/delete",
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded',
					'Accept' : 'application/json',
					'Authorization-Token' : $scope.token
				}
			}).success(function(data, status, headers, config) {
				if (data.success) {
					$rootScope.categories.splice(categoryIndex, 1);
				} else {
					$scope.message = 'Deletion of category "' + $scope.categories[categoryIndex].categoryType + '" has failed!';
					$scope.showErrorMessage = true;
				}
			}).error(function(response, status, headers, config) {
				$scope.message = 'Deletion of category "' + $scope.categories[categoryIndex].categoryType + '" has failed!';
				$scope.showErrorMessage = true;
			});
		}
	};


	function getCategoryIndex (categoryId) {
		for (var i = 0; i < $rootScope.categories.length; ++i) {
			if ($rootScope.categories[i].categoryId == categoryId) {
				return i;
			}
		}

		return -1;
	}
});
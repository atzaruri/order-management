app.controller('adminController', function ($scope, $rootScope, $http) {
	var jsonToken = sessionStorage.token;

	if (jsonToken == null) {
		window.location.href = '/order.management/login.html'
	}

	var user = JSON.parse(sessionStorage.user);

	if (user.role != 'Admin') {
		window.location.href = '/order.management/home.html'
	}

	$scope.token = JSON.parse(jsonToken);
	$scope.controller = getIncludedPageController();

	$scope.getIncludedPage = function () {
		var questionMark = window.location.href.indexOf("?");
		if (questionMark < 0) return;

		var tabName = window.location.href.slice(questionMark + 1, window.location.href.length);
		return '/order.management/admin/' + tabName + '.html';
	};

	function getIncludedPageController() {
		var questionMark = window.location.href.indexOf("?");
		if (questionMark < 0) return;

		var tabName = window.location.href.slice(questionMark + 1, window.location.href.length);
		return tabName + 'Controller'
	};
});
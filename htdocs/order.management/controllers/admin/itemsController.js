app.controller('itemsController', function ($scope, $rootScope, $http, ngDialog) {
	$scope.token = JSON.parse(sessionStorage.token);
	$rootScope.item = $rootScope.itemIndex = null;

	/* Get categories ------------------------------------------------------------------------------------------------------ */
	$http({
		method : 'GET',
		url : 'http://localhost:8080/order.management/categories',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded',
			'Accept' : 'application/json',
			'Authorization-Token' : $scope.token
		}
	}).success(function (data, status, headers, config) {
		$scope.categories = data;
	}).error(function (response, status, headers, config) {
		$scope.categories = ['error'];
	});

	/* Get all items ------------------------------------------------------------------------------------------------------ */
	$http({
		method : 'GET',
		url : 'http://localhost:8080/order.management/items',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded',
			'Accept' : 'application/json',
			'Authorization-Token' : $scope.token
		}
	}).success(function (data, status, headers, config) {
		$rootScope.items = data;
	}).error(function (response, status, headers, config) {
		$rootScope.items = [];
	});

	$scope.openNewItem = function () {
        ngDialog.open({ template: '/order.management/admin/newUpdateItem.html', controller: 'newUpdateItemController' });
    };

    $scope.openUpdateItem = function (itemId) {
    	$rootScope.itemIndex = getItemIndex (itemId);
        ngDialog.open({ template: '/order.management/admin/newUpdateItem.html', controller: 'newUpdateItemController' });
    };

    $scope.delete = function (itemId) {
    	$scope.showErrorMessage = false;
    	var itemIndex = getItemIndex(itemId);

    	if (itemIndex >= 0) {
			$http({
	    		method : 'POST',
	    		url : 'http://localhost:8080/order.management/admin/items/' + itemId + "/delete",
	    		headers : {
	    			'Content-Type' : 'application/x-www-form-urlencoded',
	    			'Accept' : 'application/json',
	    			'Authorization-Token' : $scope.token
	    		}
	    	}).success(function(data, status, headers, config) {
	    		if (data.success) {
	    			$rootScope.items.splice(itemIndex, 1);
	    		} else {
	    			$scope.message = 'Deletion of item "' + $rootScope.items[itemIndex].itemName + '" has failed!';
	    			$scope.showErrorMessage = true;
	    		}
	    	}).error(function(response, status, headers, config) {
	    		$scope.message = 'Deletion of item "' + $rootScope.items[itemIndex].itemName + '" has failed!';
	    		$scope.showErrorMessage = true;
	    	});
	    }
    }

	$scope.parseFloat = function (number) {
		return parseFloat(Math.round(number * 100) / 100).toFixed(2);
	}

	function getItemIndex (itemId) {
		for (var i = 0; i < $rootScope.items.length; ++i) {
			if ($rootScope.items[i].itemId == itemId) {
				return i;
			}
		}

		return -1;
	}
});
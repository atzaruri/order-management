app.controller('newUpdateItemController', function ($scope, $rootScope, $http, ngDialog) {
	$scope.token = JSON.parse(sessionStorage.token);

	$scope.item = {};
	var itemIndex = $rootScope.itemIndex;
	$rootScope.itemIndex = null;

	if (itemIndex != null) {
		$scope.item.itemId = $rootScope.items[itemIndex].itemId;
		$scope.item.itemName = $rootScope.items[itemIndex].itemName;
		$scope.item.categoryType = $rootScope.items[itemIndex].itemCategory.categoryType;
		$scope.item.itemPrice = $rootScope.items[itemIndex].itemPrice;
	}

	/* Get categories ------------------------------------------------------------------------------------------------------ */
	$http({
		method : 'GET',
		url : 'http://localhost:8080/order.management/categories',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded',
			'Accept' : 'application/json',
			'Authorization-Token' : $scope.token
		}
	}).success(function (data, status, headers, config) {
		$scope.categories = data;
		if ($scope.categories.length > 0 && itemIndex == null) {
			$scope.item.categoryType = $scope.categories[0].categoryType;
		}
	}).error(function (response, status, headers, config) {
		$scope.categories = ['error'];
	});

	$scope.submit = function() {
		if ($scope.formValid) {
			if (itemIndex != null & itemIndex > -1) {
				updateItem ();
			} else {
				createItem ();
			}
		}
	};

	$scope.cancel = function () {
		ngDialog.close();
	};

	function createItem () {
		$scope.item.categoryId = getCategoryId($scope.item.categoryType);

		$http({
			method : 'POST',
			url : 'http://localhost:8080/order.management/admin/items',
			data : $.param($scope.item),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded',
				'Accept' : 'application/json',
				'Authorization-Token' : $scope.token
			}
		}).success(function(data, status, headers, config) {
			if (data.success) {
				var newItem = { "itemId" : data.itemId, "itemName" : $scope.item.itemName, "itemCategory" : { "categoryType" : $scope.item.categoryType}, "itemPrice" : $scope.item.itemPrice };
				$rootScope.items.push(newItem);
				ngDialog.close();
			} else {
				$scope.message = 'Item creation failed';
				$scope.showErrorMessage = true;
			}
		}).error(function(response, status, headers, config) {
			$scope.message = 'Item creation failed';
			$scope.showErrorMessage = true;
		});
	}

	function updateItem () {
		$scope.item.categoryId = getCategoryId($scope.item.categoryType);

		$http({
			method : 'POST',
			url : 'http://localhost:8080/order.management/admin/items/' + $scope.item.itemId + '/update',
			data : $.param($scope.item),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded',
				'Accept' : 'application/json',
				'Authorization-Token' : $scope.token
			}
		}).success(function(data, status, headers, config) {
			if (data.success) {
				$rootScope.items[itemIndex].itemId = $scope.item.itemId;
				$rootScope.items[itemIndex].itemName = $scope.item.itemName;
				$rootScope.items[itemIndex].itemCategory.categoryType = $scope.item.categoryType;
				$rootScope.items[itemIndex].itemPrice = $scope.item.itemPrice;
				ngDialog.close();
			} else {
				$scope.message = 'Item update failed';
				$scope.showErrorMessage = true;
			}
		}).error(function(response, status, headers, config) {
			$scope.message = 'Item update failed';
			$scope.showErrorMessage = true;
		});
	}

	function getCategoryId (categoryType) {
		for (var i = 0; i < $scope.categories.length; ++i) {
			if ($scope.categories[i].categoryType == categoryType) {
				return $scope.categories[i].categoryId;
			}
		}

		return -1;
	}

});
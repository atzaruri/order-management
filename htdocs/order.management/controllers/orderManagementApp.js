var app = angular.module("orderManagement", ['ngDialog']);

app.directive('dynController', ['$compile', '$parse', function($compile, $parse) {
	return {
		restrict: 'A',
		terminal: true,
		priority: 100000,
		link: function (scope, elem, attrs) {
	            // Parse the scope variable
	            var name = $parse(elem.attr('dyn-controller'))(scope);
	            elem.removeAttr('dyn-controller');
	            elem.attr('ng-controller', name);

	            // Compile the element with the ng-controller attribute
	            $compile(elem)(scope);       
	        }
	    };
	}]);

app.directive('ngConfirmClick', [
	function(){
		return {
			priority: -1,
			restrict: 'A',
			link: function(scope, element, attrs){
				element.bind('click', function(e){
					var message = attrs.ngConfirmClick;
					if(message && !confirm(message)){
						e.stopImmediatePropagation();
						e.preventDefault();
					}
				});
			}
		}
	}
	]);


/* Filter for items table (right panel) - filters table after item name and item category */
app.filter('itemsFilter', function (){
	return function (items, nameTerm, categoryTerm) {
		if (categoryTerm == ' All ') {
			categoryTerm = "";
		}

      // If no array is given, exit.
      if (!items) {
      	return;
      }

      // If no search term exists, return the array unfiltered.
      else if (!nameTerm && !categoryTerm) {
      	return items;
      }
      // Otherwise, continue.
      else {
           // Return the array and filter it by looking for any occurrences of the search term in each items id or name. 
           return items.filter(function (item){
           	var termInName = true;
           	if (nameTerm) termInName = item.itemName.toLowerCase().indexOf(nameTerm.toLowerCase()) > -1; 
           	var termInCategory = true;
           	if (categoryTerm) termInCategory = item.itemCategory.categoryType.indexOf(categoryTerm) > -1;
           	return termInName && termInCategory;
           });
       } 
   }    
});
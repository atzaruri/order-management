app.controller('navbarController', function ($scope, $rootScope) {
	var userJson = sessionStorage.user;

	if (userJson != null) {
		$scope.user = JSON.parse(userJson);
		$scope.loggedInUser = 'Logged in as: ' + $scope.user.name;
		if ($scope.user.role == 'Admin') {
			$scope.loggedInUser += ' (A)'
		}
	}

	$scope.showNavBarMenu = $scope.user.role == 'Admin' ? true : false;

	$rootScope.tabs = [{"name" : "users", "active" : false}, {"name" : "categories", "active" : false}, {"name" : "items", "active" : false}];

	if (stringStartsWith(window.location.href, 'http://localhost/order.management/admin/admin.html')) {
		var questionMark = window.location.href.indexOf("?");
		if (questionMark < 0) questionMark = 0;
		var tabName = window.location.href.slice(questionMark + 1, window.location.href.length);

		var found = false;

		for (var i = 0; i < $rootScope.tabs.length; ++i) {
			if ($rootScope.tabs[i].name == tabName) {
				found = true;
				$rootScope.tabs[i].active = true;
			} else {
				$rootScope.tabs[i].active = false;
			}
		}

		if (!found) window.location.href = "/order.management/admin/admin.html?" + $rootScope.tabs[2].name;
	}

	$scope.selectTab = function (tabName) {
		for (var i = 0; i < $rootScope.tabs.length; ++i) {
			if ($rootScope.tabs[i].name == tabName) {
				$rootScope.tabs[i].active = true;
			} else {
				$rootScope.tabs[i].active = false;
			}
		}

		window.location.href = "/order.management/admin/admin.html?" + tabName;
	}

	$scope.tabName = function (tabName) {
		return tabName.slice(0, 1).toUpperCase() + tabName.slice(1, tabName.length);
	}

	$scope.logout = function () {
		sessionStorage.removeItem('token');
		window.location.href = '/order.management/login.html'
	};

	function stringStartsWith (string, prefix) {
		return string.slice(0, prefix.length) == prefix;
	}
});
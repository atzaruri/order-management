app.controller('tableController', function ($scope, $rootScope, $http, ngDialog) {
	var jsonToken = sessionStorage.token;

	if (jsonToken == null) {
		window.location.href = '/order.management/login.html'
	}

	if (!stringStartsWith(window.location.href, 'http://localhost/order.management/home.html')) {
		window.location.href = '/order.management/home.html';
	}

	/* Initialization */
	$scope.token = JSON.parse(jsonToken);

	$scope.showErrorMessage = false;

	$scope.categories = [];
	$scope.items = [];
	$scope.order = null;
	$scope.orderItems = [];

	/* Get categories ------------------------------------------------------------------------------------------------------ */
	$http({
		method : 'GET',
		url : 'http://localhost:8080/order.management/categories',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded',
			'Accept' : 'application/json',
			'Authorization-Token' : $scope.token
		}
	}).success(function (data, status, headers, config) {
		$scope.categories = data;
	}).error(function (response, status, headers, config) {
		$scope.categories = ['error'];
	});

	/* Get items  ---------------------------------------------------------------------------------------------------------- */
	$http({
		method : 'GET',
		url : 'http://localhost:8080/order.management/items',
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded',
			'Accept' : 'application/json',
			'Authorization-Token' : $scope.token
		}
	}).success(function (data, status, headers, config) {
		$scope.items = data;
	}).error(function (response, status, headers, config) {
		$scope.items = [];
	});

	/* Get order for table ------------------------------------------------------------------------------------------------- */
	$http({
		method : 'GET',
		url : 'http://localhost:8080/order.management/orders/table/' + $rootScope.tableNumber,
		headers : {
			'Content-Type' : 'application/x-www-form-urlencoded',
			'Accept' : 'application/json',
			'Authorization-Token' : $scope.token
		}
		}).success(function (data, status, headers, config) {
			$scope.order = data;

			// Get order items
			if ($scope.order) {
				$http({
					method : 'GET',
					url : 'http://localhost:8080/order.management/orders/' + $scope.order.orderNumber + "/items",
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded',
						'Accept' : 'application/json',
						'Authorization-Token' : $scope.token
					}
				}).success(function (data, status, headers, config) {
					$scope.orderItems = data;
				}).error(function (response, status, headers, config) {
					$scope.orderItems = [];
				});
			}
		}).error(function (response, status, headers, config) {
			$scope.order = {};
		});

		/* Add item to order ------------------------------------------------------------------------------------------------- */
		$scope.addItemToOrder = function (itemId, quantity) {
			$scope.showErrorMessage = false;

		// If there is no open order for the current table, create one
		if (!$scope.order.orderNumber) {
			var order = { "orderNumber": 0, "tableNumber" : $rootScope.tableNumber, "orderStatus":"Open", "total" : 0};

			$http({
				method : 'POST',
				url : 'http://localhost:8080/order.management/orders',
				data : $.param(order),
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded',
					'Accept' : 'application/json',
					'Authorization-Token' : $scope.token
				}
			}).success(function (data, status, headers, config) {
				if (data.orderNumber > 0) {
					$scope.order = order;
					$scope.order.orderNumber = data.orderNumber;
					updateTableHasOrder($rootScope.tableNumber, true);
					createOrderItem(itemId, quantity)
				} else {
					$scope.showErrorMessage = true;
					$scope.message = "Order opening has failed!";
				}
			}).error(function (response, status, headers, config) {
				$scope.showErrorMessage = true;
				$scope.message = "Order opening has failed!";
			});
		} else {
			createOrderItem(itemId, quantity);
		}
		
	};

	/* Close order  ----------------------------------------------------------------------------------------------------- */
	$scope.closeOrder = function () {
		$scope.showErrorMessage = false;

		if ($scope.order) {
			$http({
				method : 'POST',
				url : 'http://localhost:8080/order.management/orders/' + $scope.order.orderNumber + '/close',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded',
					'Accept' : 'application/json',
					'Authorization-Token' : $scope.token
				}
			}).success(function (data, status, headers, config) {
				if (data.success) {
					$scope.order = null;
					updateTableHasOrder($rootScope.tableNumber, false);
					ngDialog.close();
				} else {
					$scope.showErrorMessage = true;
					$scope.message = "Order close has failed!";
				}
			}).error(function (response, status, headers, config) {
				$scope.showErrorMessage = true;
				$scope.message = "Order close has failed!";
			});
		}
	};

	/* Remove item from order --------------------------------------------------------------------------------------------- */
	$scope.removeItemFromOrder = function (itemId) {
		$scope.showErrorMessage = false;

		if ($scope.order) {
			$http({
				method : 'POST',
				url : 'http://localhost:8080/order.management/orders/' + $scope.order.orderNumber + '/items/' + itemId + '/delete',
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded',
					'Accept' : 'application/json',
					'Authorization-Token' : $scope.token
				}
			}).success(function (data, status, headers, config) {
				if (data.success) {
					for (var i = 0; i < $scope.orderItems.length; ++i) {
						if ($scope.orderItems[i].pk.item.itemId == itemId) {
							$scope.order.total -= $scope.orderItems[i].pk.item.itemPrice * $scope.orderItems[i].quantity;
							$scope.orderItems.splice(i, 1);
							break;
						}
					}
				} else {
					$scope.showErrorMessage = true;
					$scope.message = "Item remove has failed!";
				}
			}).error(function (response, status, headers, config) {
				$scope.showErrorMessage = true;
				$scope.message = "Item remove has failed!";
			});
		}
	}

	/* Create new order item  --------------------------------------------------------------------------------------------- */
	function createOrderItem(itemId, quantity) {
		var orderItem = { "orderNumber" : $scope.order.orderNumber, "itemId" : itemId, "quantity" : quantity };
		
		$http({
			method : 'POST',
			url : 'http://localhost:8080/order.management/order_items',
			data : $.param(orderItem),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded',
				'Accept' : 'application/json',
				'Authorization-Token' : $scope.token
			}
		}).success(function (data, status, headers, config) {
			if (data.success) {
				addNewOrderItemToOrderItems(itemId, quantity);
			} else {
				$scope.showErrorMessage = true;
				$scope.message = "Item add has failed!";
			}
		}).error(function (response, status, headers, config) {
			$scope.showErrorMessage = true;
			$scope.message = "Item add has failed!";
		});
	}

	$scope.parseFloat = function (number) {
		return parseFloat(Math.round(number * 100) / 100).toFixed(2);
	}

	function stringStartsWith (string, prefix) {
		return string.slice(0, prefix.length) == prefix;
	}

	function getItemById (itemId) {
		for (var i = 0; i < $scope.items.length; ++i) {
			if ($scope.items[i].itemId == itemId) {
				return $scope.items[i];
			}
		}
	}

	function addNewOrderItemToOrderItems (itemId, quantity) {
		// Search to see if the item already exists in current order
		for (var i = 0; i < $scope.orderItems.length; ++i) {
			if ($scope.orderItems[i].pk.item.itemId == itemId) {
				$scope.orderItems[i].quantity += quantity;
				$scope.order.total += $scope.orderItems[i].pk.item.itemPrice * quantity;
				return;
			}
		}

		// The item doesn't exists in the current order - create a new one
		var createdOrderItem = { "pk": { "item" : {}}, "quantity" : quantity }
		createdOrderItem.pk.item = getItemById(itemId);
		$scope.orderItems.push(createdOrderItem);
		$scope.order.total += createdOrderItem.pk.item.itemPrice * quantity;
	}

	function updateTableHasOrder (tableNumber, hasOrder) {
		for (var row = 0; row < $rootScope.tables.length; ++row) {
			for (var column = 0; column < $rootScope.tables[row].length; ++column) {
				if ($rootScope.tables[row][column].tableNumber == tableNumber) {
					$rootScope.tables[row][column].hasOrder = hasOrder;
				}
			}
		}
	}
});
app.controller('loginController', function($scope, $http) {
	$scope.showErrorMessage = false;

	var jsonToken = sessionStorage.token;

	if (jsonToken != null) {
		window.location.href = '/order.management/home.html'
	}
	
	$scope.submitLogin = function() {
		if ($scope.formValid) {
			$http({
				method : 'POST',
				url : 'http://localhost:8080/order.management/authentication',
				data : $.param($scope.user),
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded',
					'Accept' : 'application/json',
				}
			}).success(function(data, status, headers, config) {
				if (data != null && data.token != null) {
					sessionStorage.token = JSON.stringify(data.token);
					var user = {"name" : data.name, "role" : data.role};
					sessionStorage.user = JSON.stringify(user);

					if (user.role == 'Admin') {
						window.location.href = '/order.management/admin/admin.html'
					} else {
						window.location.href = '/order.management/home.html'
					}
				} else {
					$scope.message = 'Username or password is invalid!';
					$scope.showErrorMessage = true;
				}
			}).error(function(response, status, headers, config) {
				$scope.message = 'An error occured during login. Please try again!';
				$scope.showErrorMessage = true;
			});
		}
	};
});
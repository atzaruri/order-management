app.controller('homeController', function ($scope, $rootScope, $http, ngDialog) {
	var jsonToken = sessionStorage.token;

	if (jsonToken == null) {
		window.location.href = '/order.management/login.html'
	}

	$scope.token = JSON.parse(jsonToken);

	$http({
		method : 'GET',
		url : 'http://localhost:80/order.management/configurations/tables.cfg',
		headers : {
			'Accept' : 'application/json'
		}
	}).success(function (data, status, headers, config) {
		$rootScope.tables = data;

		// Set which tables have open orders
		$http({
			method : 'GET',
			url : 'http://localhost:8080/order.management/orders/open',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded',
				'Accept' : 'application/json',
				'Authorization-Token' : $scope.token
			}
		}).success(function (data, status, headers, config) {
			// loop at all tables and see if they have open orders
			for (var row = 0; row < $rootScope.tables.length; ++row) {
				for (var column = 0; column < $rootScope.tables[row].length; ++column) {
					$rootScope.tables[row][column].hasOrder = tableHasOpenOrder($rootScope.tables[row][column].tableNumber, data);
				}
			}
		}).error(function (response, status, headers, config) {});
	}).error(function (response, status, headers, config) {
		$scope.tables = {};
	});

	$scope.openTableOrder = function (tableNumber) {
		$rootScope.tableNumber = tableNumber;
        ngDialog.open({ template: 'table.html', controller: 'tableController' });
    };

    function tableHasOpenOrder (tableNumber, openOrders) {
    	for (var i = 0; i < openOrders.length; ++i) {
    		if (openOrders[i].tableNumber == tableNumber) {
    			return true;
    		}
    	}

    	return false;
    }
});